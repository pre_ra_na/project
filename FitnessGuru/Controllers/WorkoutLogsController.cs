﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FitnessGuru.Models;
using Microsoft.AspNet.Identity;

namespace FitnessGuru.Controllers
{
    public class WorkoutLogsController : Controller
    {
        private FitnessDbCM db = new FitnessDbCM();

        // GET: WorkoutLogs
        public ActionResult Index()
        {
            DateTime Time = DateTime.Now.Date;
            var workoutLogs = db.WorkoutLogs.Where(p => p.TimeStamp == Time).Include(w => w.Workout);

            return View(workoutLogs.ToList());
        }

        // GET: WorkoutLogs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WorkoutLog workoutLog = db.WorkoutLogs.Find(id);
            if (workoutLog == null)
            {
                return HttpNotFound();
            }
            return View(workoutLog);
        }

        // GET: WorkoutLogs/Create
        public ActionResult Create()
        {
            WorkoutLog model = new WorkoutLog() { UserId = GetCurrentUserId() };
            ViewBag.WorkoutId = new SelectList(db.Workouts, "Id", "WorkoutName");
            return PartialView(model);
        }

        // POST: WorkoutLogs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,UserId,WorkoutId,TimePeriod,Calorie,TimeStamp")] WorkoutLog workoutLog)
        {
            if (ModelState.IsValid)
            {
                workoutLog.UserId = GetCurrentUserId();
                workoutLog.Calorie = GetCalorie(workoutLog);
                workoutLog.TimeStamp = DateTime.Now;
                db.WorkoutLogs.Add(workoutLog);
                db.SaveChanges();
                return RedirectToAction("RemainingCalWorkout", "RemainingCalories", workoutLog);
            }
            ViewBag.UserId = GetCurrentUserId();
            ViewBag.WorkoutId = new SelectList(db.Workouts, "Id", "WorkoutName", workoutLog.WorkoutId);
            return View(workoutLog);
        }

        // GET: WorkoutLogs/Edit/5
        //public ActionResult Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    WorkoutLog workoutLog = db.WorkoutLogs.Find(id);
        //    if (workoutLog == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    ViewBag.WorkoutId = new SelectList(db.Workouts, "Id", "WorkoutName", workoutLog.WorkoutId);
        //    return View(workoutLog);
        //}

        //// POST: WorkoutLogs/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "Id,UserId,WorkoutId,TimePeriod,Calorie,TimeStamp")] WorkoutLog workoutLog)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(workoutLog).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.WorkoutId = new SelectList(db.Workouts, "Id", "WorkoutName", workoutLog.WorkoutId);
        //    return View(workoutLog);
        //}

        // GET: WorkoutLogs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WorkoutLog workoutLog = db.WorkoutLogs.Find(id);
            if (workoutLog == null)
            {
                return HttpNotFound();
            }
            return View(workoutLog);
        }

        // POST: WorkoutLogs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            WorkoutLog workoutLog = db.WorkoutLogs.Find(id);
            db.WorkoutLogs.Remove(workoutLog);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        public Guid GetCurrentUserId()
        { //to allow user to view only their details
            return new Guid(User.Identity.GetUserId());
        }
        public bool EnsureIsUserContact(UserInfo userInfo)
        {
            return userInfo.UserId == GetCurrentUserId();
        }
        public float GetCalorie(WorkoutLog workoutLog)
        {
            var workout = db.Workouts.Find(workoutLog.WorkoutId);

            float calorie = workoutLog.TimePeriod * workout.Calories;
            return calorie;
        }
    }
}
