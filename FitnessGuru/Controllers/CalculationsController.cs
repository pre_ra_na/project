﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FitnessGuru.Models;
using Microsoft.AspNet.Identity;

namespace FitnessGuru.Controllers
{
    public class CalculationsController : Controller
    {
        private FitnessDbCM db = new FitnessDbCM();

        // GET: Calculations/Details/5
        public ActionResult Details()
        {
            Guid userid = GetCurrentUserId();

            var calcalc = db.Calc.Where(p => p.UserId == userid).FirstOrDefault();

            return PartialView(calcalc);
        }

        // GET: Calculations/Create
        public ActionResult Calculate(UserInfo userInfo)
        {
            Calculation calc = new Calculation();
            calc.UserId = userInfo.UserId;
            calc.BMI = calc.BmiCalc(userInfo);
            calc.BMR = calc.BmrCalc(userInfo);
            calc.BMRperDay = calc.BmrPerDayCalc(userInfo, calc);
            db.Calc.Add(calc);
            db.SaveChanges();
            return View(calc);
        }

          protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public Guid GetCurrentUserId()
        { //to allow user to view only their details
            return new Guid(User.Identity.GetUserId());
        }
    }
}
