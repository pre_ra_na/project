﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FitnessGuru.Models;

namespace FitnessGuru.Controllers
{
    public class FoodModelsController : Controller
    {
        private FitnessDbCM db = new FitnessDbCM();

        // GET: FoodModels
        public ActionResult Index()
        {
            return View(db.Foods.ToList());
        }

        // GET: FoodModels/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FoodModel foodModel = db.Foods.Find(id);
            if (foodModel == null)
            {
                return HttpNotFound();
            }
            return View(foodModel);
        }

        // GET: FoodModels/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: FoodModels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,FoodName,ServingSize,Calories,Carbohydrate,Fat,Protein")] FoodModel foodModel)
        {
            if (ModelState.IsValid)
            {
                db.Foods.Add(foodModel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(foodModel);
        }

        // GET: FoodModels/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FoodModel foodModel = db.Foods.Find(id);
            if (foodModel == null)
            {
                return HttpNotFound();
            }
            return View(foodModel);
        }

        // POST: FoodModels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,FoodName,ServingSize,Calories,Carbohydrate,Fat,Protein")] FoodModel foodModel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(foodModel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(foodModel);
        }

        // GET: FoodModels/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FoodModel foodModel = db.Foods.Find(id);
            if (foodModel == null)
            {
                return HttpNotFound();
            }
            return View(foodModel);
        }

        // POST: FoodModels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            FoodModel foodModel = db.Foods.Find(id);
            db.Foods.Remove(foodModel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult Search(string search)
        {                   
                return View(db.Foods.Where(x => x.FoodName.StartsWith(search) || search == null).ToList());
           
        }
    }
}
