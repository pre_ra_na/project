﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FitnessGuru.Models;
using Microsoft.AspNet.Identity;

namespace FitnessGuru.Controllers
{
    [Authorize]
    public class UserInfoesController : Controller
    {
        private FitnessDbCM db = new FitnessDbCM();

        // GET: UserInfoes
        public ActionResult Index()
        {
            var userId = GetCurrentUserId();
            return View(db.Users.Where(x=>x.UserId== userId).ToList());
        }

        // GET: UserInfoes/Details/5
        public ActionResult Details()
        {
            var userid = GetCurrentUserId();
            UserInfo userInfo = db.Users.Where(p=>p.UserId == userid).FirstOrDefault();

            if (userInfo == null || !EnsureIsUserContact(userInfo))
            {
                return HttpNotFound();
            }
            return View(userInfo);
        }

        // GET: UserInfoes/Create
        public ActionResult Create()
        {
            ViewBag.UserId = GetCurrentUserId();
            var vm = new UserInfo();
            // Hard coded for demo. You can replace with real data from db
            vm.Activeness = new List<SelectListItem> {
                new SelectListItem { Value ="1", Text="Sedentary" },
                new SelectListItem { Value ="2", Text="Lightly Active" },
                new SelectListItem { Value ="3", Text="Moderately Active" },
                new SelectListItem { Value ="4", Text="Very Active" },
                new SelectListItem { Value ="5", Text="Extremely Active" }
                  };
            vm.ActivenessId = 1;

            return View(vm);
        }

        // POST: UserInfoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,UserId,FName,LName,Address,PhoneNumber,RegisteredDate,Gender,Birthday,Age,Height,CurrentWt,Goalwt,ActivenessId")] UserInfo userInfo)
        {
            if (ModelState.IsValid)
            {
                userInfo.UserId = GetCurrentUserId();
                userInfo.RegisteredDate = DateTime.Now;
                userInfo.Age = GetAge(userInfo);
                db.Users.Add(userInfo);
                db.SaveChanges();
                return RedirectToAction("Calculate","Calculations", userInfo);
            }
            ViewBag.UserId = GetCurrentUserId();
            userInfo.Activeness = new List<SelectListItem> {
                new SelectListItem { Value ="1", Text="Sedentary" },
                new SelectListItem { Value ="2", Text="Lightly Active" },
                new SelectListItem { Value ="3", Text="Moderately Active" },
                new SelectListItem { Value ="4", Text="Very Active" },
                new SelectListItem { Value ="5", Text="Extremely Active" }
                  };
            userInfo.ActivenessId = 1;
            return View(userInfo);
        }

        // GET: UserInfoes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserInfo userInfo = db.Users.Find(id);
            if (userInfo == null || !EnsureIsUserContact(userInfo))
            {
                return HttpNotFound();
            }
            userInfo.Activeness = new List<SelectListItem> {
                new SelectListItem { Value ="1", Text="Sedentary" },
                new SelectListItem { Value ="2", Text="Lightly Active" },
                new SelectListItem { Value ="3", Text="Moderately Active" },
                new SelectListItem { Value ="4", Text="Very Active" },
                new SelectListItem { Value ="5", Text="Extremely Active" }
                  };
            userInfo.ActivenessId = 1;

            return View(userInfo);
        }

        // POST: UserInfoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,UserId,FName,LName,Address,PhoneNumber,RegisteredDate,Gender,Birthday,Age,Height,CurrentWt,Goalwt,ActivenessId")] UserInfo userInfo)
        {
            if (ModelState.IsValid)
            {
                db.Entry(userInfo).State = EntityState.Modified;
                userInfo.UserId = GetCurrentUserId();
                    
                userInfo.Age = GetAge(userInfo);
                db.SaveChanges();
                return RedirectToAction("Calculate", "Calculations", userInfo);
            }
            userInfo.Activeness = new List<SelectListItem> {
                new SelectListItem { Value ="1", Text="Sedentary" },
                new SelectListItem { Value ="2", Text="Lightly Active" },
                new SelectListItem { Value ="3", Text="Moderately Active" },
                new SelectListItem { Value ="4", Text="Very Active" },
                new SelectListItem { Value ="5", Text="Extremely Active" }
                  };
            userInfo.ActivenessId = 1;
            ViewBag.UserId = GetCurrentUserId();
            return View(userInfo);
        }

        // GET: UserInfoes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserInfo userInfo = db.Users.Find(id);
            if (userInfo == null || !EnsureIsUserContact(userInfo))
            {
                return HttpNotFound();
            }
            return View(userInfo);
        }

        // POST: UserInfoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            UserInfo userInfo = db.Users.Find(id);
            if (!EnsureIsUserContact(userInfo))
            {
                return HttpNotFound();
            }
            db.Users.Remove(userInfo);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        public Guid GetCurrentUserId()
        { //to allow user to view only their details
            return new Guid(User.Identity.GetUserId());
        }
        public bool EnsureIsUserContact(UserInfo userInfo)
        {
            return userInfo.UserId == GetCurrentUserId();
        }
        public int GetAge(UserInfo userInfo)
        {
            // Save today's date.
            var today = DateTime.Today;
            // Calculate the age.
            var age = today.Year - userInfo.Birthday.Year;
            // Go back to the year the person was born in case of a leap year
            if (userInfo.Birthday > today.AddYears(-age)) age--;
            return age;
        }

        public ActionResult showInfo()
        {
            var userId = GetCurrentUserId();
            var userInfo = db.Users.Where(p => p.UserId == userId).FirstOrDefault();
            return PartialView(userInfo);
        }

    }
}
