﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FitnessGuru.Models;
using Microsoft.AspNet.Identity;

namespace FitnessGuru.Controllers
{
    [Authorize]
    public class RemainingCaloriesController : Controller
    {
       
        private FitnessDbCM db = new FitnessDbCM();
        public ActionResult showRemCal()
        {
            DateTime Time = DateTime.Now.Date;
            Guid userid = new Guid(User.Identity.GetUserId());
            var remcal = db.RemCal.OrderByDescending(p => p.id).Where(p => p.Time == Time && p.UserId==userid).FirstOrDefault();
            return PartialView(remcal);
        }
        // GET: RemainingCalories
        public ActionResult Index()
        {
            DateTime Time = DateTime.Now.Date;
            var remcal = db.RemCal.Where(p => p.Time == Time);
            return View(remcal.ToList());
        }

        // GET: RemainingCalories/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RemainingCalories remainingCalories = db.RemCal.Find(id);
            if (remainingCalories == null)
            {
                return HttpNotFound();
            }
            return View(remainingCalories);
        }
        public ActionResult RemainingCal(DailyLog dailyLog)
        {
            RemainingCalories remCalories = new RemainingCalories();
            Calculation calc = new Calculation();
            remCalories.UserId = new Guid(User.Identity.GetUserId());
            remCalories.Time = DateTime.Now.Date;
            //var dailylogs = db.DailyLogs.Where(p => p.UserId == remCalories.UserId && p.TimeStamp == remCalories.Time).FirstOrDefault();
            
            var calcalc = db.Calc.Where(p => p.UserId == remCalories.UserId).FirstOrDefault();
            var rem = db.RemCal.OrderByDescending(p => p.id).Where(p => p.UserId == remCalories.UserId && p.Time == remCalories.Time).FirstOrDefault();
            //var totalCalinDay = dailylogs.Sum(p => p.Calorie);

            //take out the calorie and minus it 
            if (rem == null)
            {
                remCalories.RemainingCal = calcalc.BMRperDay;
                remCalories.TotalCarbohydrate = 0;
                remCalories.TotalFat = 0;
                remCalories.TotalProtein = 0;
            }
            else
            {
                var res = db.RemCal.OrderByDescending(p => p.id).Where(p => p.UserId == remCalories.UserId && p.Time == remCalories.Time).FirstOrDefault();
                remCalories.RemainingCal = res.RemainingCal;
                remCalories.TotalCarbohydrate = res.TotalCarbohydrate;
                remCalories.TotalFat = res.TotalFat;
                remCalories.TotalProtein = res.TotalProtein;
            }
            remCalories.RemainingCal = remCalories.RemainingCal - dailyLog.Calorie;
            remCalories.TotalCarbohydrate = remCalories.TotalCarbohydrate + dailyLog.TotalCarbohydrate;
            remCalories.TotalFat = remCalories.TotalFat + dailyLog.TotalFat;
            remCalories.TotalProtein = remCalories.TotalProtein + dailyLog.TotalProtein;
            db.RemCal.Add(remCalories);
            db.SaveChanges();
            return RedirectToAction("Index");


        }

        public ActionResult RemainingCalWorkout(WorkoutLog workoutLog)
        {
            RemainingCalories remCalories = new RemainingCalories();
            Calculation calc = new Calculation();
            remCalories.UserId = new Guid(User.Identity.GetUserId());
            remCalories.Time = DateTime.Now.Date;
           
            var calcalc = db.Calc.Where(p => p.UserId == remCalories.UserId).FirstOrDefault();
            var rem = db.RemCal.OrderByDescending(p => p.id).Where(p=>p.UserId== remCalories.UserId && p.Time == remCalories.Time).FirstOrDefault();
            //take out the calorie and minus it 
            if (rem == null)
            {
                remCalories.RemainingCal = calcalc.BMRperDay;
            }
            else
            {
                var res = db.RemCal.OrderByDescending(p => p.id).Where(p => p.UserId == remCalories.UserId && p.Time == remCalories.Time).FirstOrDefault();
                remCalories.RemainingCal = res.RemainingCal;
            }
            remCalories.RemainingCal = remCalories.RemainingCal + workoutLog.Calorie;

            db.RemCal.Add(remCalories);
            db.SaveChanges();
            return RedirectToAction("Index");


        }
        // GET: RemainingCalories/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RemainingCalories remainingCalories = db.RemCal.Find(id);
            if (remainingCalories == null)
            {
                return HttpNotFound();
            }
            return View(remainingCalories);
        }

        // POST: RemainingCalories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            RemainingCalories remainingCalories = db.RemCal.Find(id);
            db.RemCal.Remove(remainingCalories);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult Piechart()
        {
            DateTime Time = DateTime.Now.Date;
            Guid userid = new Guid(User.Identity.GetUserId());
            var remcal = db.RemCal.OrderByDescending(p => p.id).Where(p => p.Time == Time && p.UserId == userid).FirstOrDefault();
            return View(remcal);
        }

    }
}
