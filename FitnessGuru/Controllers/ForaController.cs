﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FitnessGuru.Models;
using Microsoft.AspNet.Identity;

namespace FitnessGuru.Controllers
{
    public class ForaController : Controller
    {
        private FitnessDbCM db = new FitnessDbCM();

        // GET: Fora
        public ActionResult Index()
        {
            return View(db.Forums.ToList());
        }

        public ActionResult QuesAns()
        {
            return View(db.Forums.ToList());
        }
        public ActionResult portletPartial(Models.Forum model)
        {
            return PartialView(model);
        }

        // GET: Fora/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Forum forum = db.Forums.Find(id);
            if (forum == null)
            {
                return HttpNotFound();
            }
            return View(forum);
        }

        // GET: Fora/Create
        public ActionResult Create()
        {
            Forum forum= new Forum();
            forum.UserId = GetCurrentUserId();
            var userInfo = db.Users.Where(p => p.UserId == forum.UserId).FirstOrDefault();
            forum.UserName = userInfo.FName + " " + userInfo.LName;
            forum.TimeofPost = DateTime.Now;

            return PartialView(forum);
        }

        // POST: Fora/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,UserId,Content,TimeofPost,ReplyId")] Forum forum)
        {
            if (ModelState.IsValid)
            {
                forum.UserId = GetCurrentUserId();
                forum.TimeofPost = DateTime.Now;
                db.Forums.Add(forum);
                db.SaveChanges();
                return RedirectToAction("QuesAns");
            }

            return View(forum);
        }



        public ActionResult Reply(int? id)
        {
            Forum model = new Forum() { UserId = GetCurrentUserId() };
            model.ReplyId = id;
            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Reply(Forum forum)
        {
            if (ModelState.IsValid)
            {
                forum.UserId = GetCurrentUserId();
                forum.TimeofPost = DateTime.Now;
                db.Forums.Add(forum);
                db.SaveChanges();
                return RedirectToAction("QuesAns");
            }

            return View(forum);
        }


        // GET: Fora/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Forum forum = db.Forums.Find(id);
            if (forum == null)
            {
                return HttpNotFound();
            }
            return View(forum);
        }

        // POST: Fora/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,UserId,Content,TimeofPost,ReplyId")] Forum forum)
        {
            if (ModelState.IsValid)
            {
                db.Entry(forum).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("QuesAns");
            }
            return View(forum);
        }

        // GET: Fora/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Forum forum = db.Forums.Find(id);
            if (forum == null)
            {
                return HttpNotFound();
            }
            return View(forum);
        }

        // POST: Fora/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Forum forum = db.Forums.Find(id);
            db.Forums.Remove(forum);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        public Guid GetCurrentUserId()
        { //to allow user to view only their details
            return new Guid(User.Identity.GetUserId());
        }
        public bool EnsureIsUserContact(UserInfo userInfo)
        {
            return userInfo.UserId == GetCurrentUserId();
        }
    }
}
