﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FitnessGuru.Models
{
    public class TimePeriod
    {
        [Key]
        public int TimeId { get; set; }
        public string PeriodName { get; set; }
    }
}