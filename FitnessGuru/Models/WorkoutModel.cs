﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FitnessGuru.Models
{
    public class WorkoutModel
    {
        [Column("WorkoutId")]
        public int Id { get; set; }
        public string WorkoutName { get; set; }
        public float Calories { get; set; }
    }
}