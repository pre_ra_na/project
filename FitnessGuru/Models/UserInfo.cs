﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FitnessGuru.Models
{
    public class UserInfo
    {
        public int Id { get; set; }
        public Guid UserId { get; set; }
        [Required]
        [Display(Name = "First Name")]
        public string FName { get; set; }
        [Required]
        [Display(Name = "Last Name")]
        public string LName { get; set; }
        [Display(Name = "Address")]
        public string Address { get; set; }
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }
        public DateTime date;
        public DateTime RegisteredDate {
            get {return date; }
            set {this.date = DateTime.Now; }
        }
        [Required]
        [Display(Name = "Gender")]
        public Gender Gender { get; set; }
        [Required]
        [Display(Name = "Date of Birth")]
        public DateTime Birthday { get; set; }
        public int Age { get; set; }
        [Required]
        [Display(Name = "Your height")]
        public int Height { get; set; }
        [Required]
        [Display(Name = "Your Current Weight")]
        public int CurrentWt { get; set; }
        [Required]
        [Display(Name = "Your Goal Weight")]
        public int Goalwt { get; set; }
        [Display(Name = "How much active are you")]
        public IEnumerable<SelectListItem> Activeness { set; get; }
        public int ActivenessId { set; get; }
    }
    public enum Gender
    {
        Male,
        Female
    }
    
}
