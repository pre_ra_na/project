﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FitnessGuru.Models
{
    public class RemainingCalories
    {
        public int id { get; set; }
        public Guid UserId { get; set; }
        [Display(Name = "Remaining Calories For Today")]
        public double RemainingCal { get; set; }
        public float? TotalCarbohydrate { get; set; }
        public float? TotalFat { get; set; }
        public float? TotalProtein { get; set; }
        public DateTime Time { get; set; }
    }
}