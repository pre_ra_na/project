﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FitnessGuru.Models
{
    public class WorkoutLog
    {
        public int Id { get; set; }
        public Guid UserId { get; set; }
        [Required]
        [Display(Name = "Name of Workout")]
        public int WorkoutId { get; set; }
        [ForeignKey("WorkoutId")]
        public virtual WorkoutModel Workout { get; set; }
        [Required]
        [Display(Name = "For how long?")]
        public float TimePeriod { get; set; }
        public double Calorie { get; set; }
        public DateTime TimeStamp { get; set; }
    }
}