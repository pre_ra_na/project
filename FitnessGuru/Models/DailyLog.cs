﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FitnessGuru.Models
{
    public class DailyLog
    {
        public int Id { get; set; }
        public Guid UserId { get; set; }
        [Required]
        [Display(Name = "When did you eat?")]
        public int TimeId { get; set; }
        [ForeignKey("TimeId")]
        public virtual TimePeriod Period { get; set; }
        [Required]
        [Display(Name = "What did you eat?")]
        public int FoodId { get; set; }
        [ForeignKey("FoodId")]
        public virtual FoodModel Food { get; set; }
        [Required]
        [Display(Name = "How much did you eat?")]
        public float Quantity { get; set; }
        public float? TotalCarbohydrate { get; set; }
        public float? TotalFat { get; set; }
        public float? TotalProtein { get; set; }
        public double Calorie { get; set; }
        public DateTime TimeStamp { get; set; }
        
    }

    
}