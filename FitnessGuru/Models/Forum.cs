﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FitnessGuru.Models
{
    public class Forum
    {
        public int Id { get; set;}
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public string Content { get; set; }
        public DateTime TimeofPost { get; set; }
        public int? ReplyId { get; set; }
     }
}