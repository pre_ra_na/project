﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FitnessGuru.Models
{
    public class FoodModel
    {   [Column("FoodId")]
        public int Id { get; set; }
        public string FoodName { get; set; }
        public string ServingSize { get; set; }
        public float Calories { get; set; }
        public float Carbohydrate { get; set; }
        public float Fat { get; set; }
        public float Protein { get; set; }

    }
}