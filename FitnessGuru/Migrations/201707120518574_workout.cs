namespace FitnessGuru.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class workout : DbMigration
    {
        public override void Up()
        {
         
            
            CreateTable(
                "dbo.WorkoutModels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        WorkoutName = c.String(),
                        Calories = c.Single(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
           
            DropTable("dbo.WorkoutModels");
          
        }
    }
}
