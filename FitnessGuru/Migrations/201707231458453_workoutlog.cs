namespace FitnessGuru.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class workoutlog : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.WorkoutModels", name: "Id", newName: "WorkoutId");
            CreateTable(
                "dbo.WorkoutLogs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Guid(nullable: false),
                        WorkoutId = c.Int(nullable: false),
                        TimePeriod = c.Single(nullable: false),
                        Calorie = c.Double(nullable: false),
                        TimeStamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.WorkoutModels", t => t.WorkoutId, cascadeDelete: true)
                .Index(t => t.WorkoutId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.WorkoutLogs", "WorkoutId", "dbo.WorkoutModels");
            DropIndex("dbo.WorkoutLogs", new[] { "WorkoutId" });
            DropTable("dbo.WorkoutLogs");
            RenameColumn(table: "dbo.WorkoutModels", name: "WorkoutId", newName: "Id");
        }
    }
}
