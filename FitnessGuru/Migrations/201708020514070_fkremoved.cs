namespace FitnessGuru.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fkremoved : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Fora", "ReplyId", "dbo.Fora");
            DropIndex("dbo.Fora", new[] { "ReplyId" });
        }
        
        public override void Down()
        {
            CreateIndex("dbo.Fora", "ReplyId");
            AddForeignKey("dbo.Fora", "ReplyId", "dbo.Fora", "Id");
        }
    }
}
