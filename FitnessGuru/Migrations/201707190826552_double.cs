namespace FitnessGuru.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _double : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.DailyLogs", "Calorie", c => c.Double(nullable: false));
            AlterColumn("dbo.RemainingCalories", "RemainingCal", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.RemainingCalories", "RemainingCal", c => c.Single(nullable: false));
            AlterColumn("dbo.DailyLogs", "Calorie", c => c.Single(nullable: false));
        }
    }
}
