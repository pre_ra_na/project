namespace FitnessGuru.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class piechart : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.RemainingCalories", "TotalCarbohydrate", c => c.Single());
            AddColumn("dbo.RemainingCalories", "TotalFat", c => c.Single());
            AddColumn("dbo.RemainingCalories", "TotalProtein", c => c.Single());
        }
        
        public override void Down()
        {
            DropColumn("dbo.RemainingCalories", "TotalProtein");
            DropColumn("dbo.RemainingCalories", "TotalFat");
            DropColumn("dbo.RemainingCalories", "TotalCarbohydrate");
        }
    }
}
