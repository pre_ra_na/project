namespace FitnessGuru.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class piechart2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DailyLogs", "TotalCarbohydrate", c => c.Single());
            AddColumn("dbo.DailyLogs", "TotalFat", c => c.Single());
            AddColumn("dbo.DailyLogs", "TotalProtein", c => c.Single());
        }
        
        public override void Down()
        {
            DropColumn("dbo.DailyLogs", "TotalProtein");
            DropColumn("dbo.DailyLogs", "TotalFat");
            DropColumn("dbo.DailyLogs", "TotalCarbohydrate");
        }
    }
}
