namespace FitnessGuru.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Forumuser : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Fora", "UserName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Fora", "UserName");
        }
    }
}
