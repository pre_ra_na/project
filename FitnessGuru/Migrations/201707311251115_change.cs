namespace FitnessGuru.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class change : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.UserInfoes", "FName", c => c.String(nullable: false));
            AlterColumn("dbo.UserInfoes", "LName", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.UserInfoes", "LName", c => c.String());
            AlterColumn("dbo.UserInfoes", "FName", c => c.String());
        }
    }
}
