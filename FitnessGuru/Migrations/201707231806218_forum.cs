namespace FitnessGuru.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class forum : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Fora",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Guid(nullable: false),
                        Content = c.String(),
                        TimeofPost = c.DateTime(nullable: false),
                        ReplyId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Fora", t => t.ReplyId)
                .Index(t => t.ReplyId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Fora", "ReplyId", "dbo.Fora");
            DropIndex("dbo.Fora", new[] { "ReplyId" });
            DropTable("dbo.Fora");
        }
    }
}
