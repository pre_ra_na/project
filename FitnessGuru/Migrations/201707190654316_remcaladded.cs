namespace FitnessGuru.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class remcaladded : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.RemainingCalories",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        UserId = c.Guid(nullable: false),
                        RemainingCal = c.Single(nullable: false),
                        Time = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            AddColumn("dbo.DailyLogs", "TimeStamp", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.DailyLogs", "TimeStamp");
            DropTable("dbo.RemainingCalories");
        }
    }
}
